"""
Coffee Breakout - a breakout clone for pygame.
"""

import sys
import pygame
import LevelDefines
import sqlite3
from numpy import *
from itertools import cycle
from random import randint
from os import listdir

# CONSTANTS AND GLOBAL VARS
WIN_SIZE = [250, 350]
MAX_FRAME_RATE = 60
SPEED_RATE = MAX_FRAME_RATE / 30
BALL_INITIAL_SPEED = [SPEED_RATE, -SPEED_RATE]
PADDLE_INITIAL_SPEED = SPEED_RATE * 2
COL_BACKGROUND = 0, 0, 0
MAX_PLAYER_LIFE = 3
MUSIC_PATH = "res/music/"

# bias for collision checks
BALL_RECT_BIAS = 2

# init sprites
ball_paddle_sprites = pygame.sprite.Group()
brick_sprites = pygame.sprite.Group()

# init sound mixer
pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.init()
# music
pygame.mixer.music.load("res/music/Agent_Six.ogg")
# set sound effects
bounce_effect = pygame.mixer.Sound("res/sound/bounce.wav")
brick_effect = pygame.mixer.Sound("res/sound/brick.wav")
# actual music volume
music_volume = 1.0
# tracker music list
track_list = []

# init pygame and set window mode
pygame.init()
pygame.display.set_caption('C-Breakout 1.0')
window = pygame.display.set_mode(WIN_SIZE)

# set clock for game ticks
clock = pygame.time.Clock()
# timers for rotate and blinking text
last_time_rotate = clock.get_time()
last_time_blink = clock.get_time()
blink_time_diff = 0

# init stats
bricks_count = 0
bricks_gone = 0

# brick id matrix and static brick position matrices
brick_position_matrix_x = zeros((LevelDefines.BRICK_LAYOUT_ROWS, LevelDefines.BRICK_LAYOUT_COLS), dtype='int')
brick_position_matrix_y = zeros((LevelDefines.BRICK_LAYOUT_ROWS, LevelDefines.BRICK_LAYOUT_COLS), dtype='int')
brick_id_matrix = zeros((LevelDefines.BRICK_LAYOUT_ROWS, LevelDefines.BRICK_LAYOUT_COLS), dtype='int')

# actual level
actual_level_num = 1

# blink event
TITLE_BLINK_EVENT = pygame.USEREVENT + 0
# song ended event
SONG_FINISHED = pygame.USEREVENT + 1
pygame.mixer.music.set_endevent(SONG_FINISHED)

# bools for menu handling
is_game_over = False
is_game_intro = True
is_running = True
is_game = False
is_highscore = False

# colors for cycle on title text
title_colors = cycle([(234, 178, 114), (229, 71, 68), (165, 68, 229), (68, 84, 221), (196, 221, 68)])
active_color = next(title_colors)
next_color = next(title_colors)
current_color = active_color
col_cycle_step = 1

# test
highscore = 0

# set window icon
try:
    icon = pygame.image.load("res/img/iCoffee.png").convert_alpha()
    pygame.display.set_icon(icon)
except FileNotFoundError:
    print("Couldn't load window icon! File not found.")
except Exception:
    print("Unknown error while trying to load icon.")


# CLASSES
class Player:
    """ Player class.

        Contains information and methods
        to handle lifes and highscore.
    """

    # constructor
    def __init__(self):
        self.life = MAX_PLAYER_LIFE
        self.score = 0

    def update_score(self, score):
        self.score = score


class Ball(pygame.sprite.Sprite):
    """
    Class for a ball object. (Sprite)
    """

    # constructor
    def __init__(self, initial_pos):
        # initialize sprite class object
        pygame.sprite.Sprite.__init__(self)
        # assign image for ball
        self.image = pygame.image.load("res/img/BallOrangeSmall.png").convert_alpha()
        self.rect = self.image.get_rect()
        # initial speed
        self.speed = BALL_INITIAL_SPEED
        # set initial position
        self.rect.x = initial_pos[0]
        self.rect.y = initial_pos[1]
        # check whether ball is in initial_pos or not
        self.moving = False
        # add ball sprite to sprite group
        ball_paddle_sprites.add(self)

    # update sprite
    def update(self):
        if self.moving:
            pos = self.rect.move(self.speed)
            self.rect = pos

    # collision check with walls
    def check_bounds(self):
        """
        Checks if the ball hits the walls of the game field.

        :return: True if ball hits a boundary, False if not.
        """
        # check left/right walls
        if self.rect.left < 0 or self.rect.right > WIN_SIZE[0]:
            self.speed[0] = -self.speed[0]
            bounce_effect.play()
        # check top/bottom walls
        if self.rect.top < 0 or self.rect.bottom > WIN_SIZE[1]:
            self.speed[1] = -self.speed[1]
            bounce_effect.play()

    # collision check with paddle
    def check_collision_paddle(self, paddle):
        """
        Checks collision of the ball with the paddle.

        :param paddle: the paddle object.
        :return: True if ball collides with paddle, False if not.
        """
        # shortcuts for side checks
        left_side = self.rect.right + self.rect.width / 2 >= paddle.rect.left
        right_side = self.rect.left + self.rect.width / 2 >= paddle.rect.right
        above_paddle = self.rect.bottom > paddle.rect.top > self.rect.bottom - self.rect.height / 2
        below_paddle = self.rect.bottom - self.rect.height / 2 >= paddle.rect.top
        corner_left = self.rect.right >= paddle.rect.left >= self.rect.right - self.rect.width / 2
        corner_right = self.rect.left <= paddle.rect.right <= self.rect.left + self.rect.width / 2
        from_left = self.speed[0] > 0
        from_right = self.speed[0] < 0

        # check if collision with paddle happened
        if pygame.sprite.collide_rect(self, paddle):
            # side checks
            if above_paddle:
                # collision at corner of the paddle
                if (corner_left and from_left) or (corner_right and from_right):
                    self.speed[0] = -self.speed[0]
                self.speed[1] = -self.speed[1]
            # collision at sides of paddle
            elif below_paddle:
                if left_side or right_side:
                    self.speed[0] = -self.speed[0]
            return True
        return False

    def check_ball_out(self, paddle):
        """
        Checks whether the ball went out or not.

        :param paddle: the paddle object.
        :return: True if the ball is below the paddle, False if not.
        """
        if self.rect.bottom - self.rect.height / 2 >= paddle.rect.top:
            return True
        return False

    def check_collision_brick(self, brick):
        """
        Checks if the ball hit any brick(s).

        :param brick: the brick object to test collision for.
        :return: True if collision happened, False if not.
        """
        if pygame.sprite.collide_rect(self, brick):

            global bricks_gone

            # skip detection if brick was already destroyed
            if brick.status == LevelDefines.BRICK_DESTR:
                return False

            # shortcuts for (possible) ball positions
            from_left_right = (self.rect.x + self.rect.width - BALL_RECT_BIAS <= brick.rect.x
                               or self.rect.x + BALL_RECT_BIAS >= brick.rect.x + brick.rect.width) \
                              and (brick.rect.y <= self.rect.y <= brick.rect.y + brick.rect.height)

            from_top_bottom = (self.rect.y <= brick.rect.y
                               or self.rect.y + BALL_RECT_BIAS >= brick.rect.y + brick.rect.height) \
                              and (brick.rect.x <= self.rect.x <= brick.rect.x + brick.rect.width)

            # check ball reflection direction
            if from_top_bottom:
                self.speed[1] *= -1
            elif from_left_right:
                self.speed[0] *= -1
            else:
                self.speed[0] *= -1
                self.speed[1] *= -1

            # play sound
            brick_effect.play()

            # delete brick
            if not brick.status >= LevelDefines.BRICK_UNDESTR:
                if not brick.status > LevelDefines.BRICK_NORMAL:
                    bricks_gone += 1
                brick.change_status(brick.status - 1)
                return True
            return False


class Brick(pygame.sprite.Sprite):
    """
    Class for a brick object. (Sprite)
    """

    def __init__(self, brick_type, position, brick_id):
        # initialize sprite class object
        pygame.sprite.Sprite.__init__(self)
        # assign image for brick
        # types --> 0 = destroyed
        #           1 = normal
        #           2 = advanced
        #           3 = undestroyable
        #           4 = special
        self.image = pygame.image.load(LevelDefines.BRICK_IMAGE[brick_type]).convert_alpha()
        # if type == 'normal':
        # self.image = pygame.image.load("res/img/BrickBlue.png").convert_alpha()
        self.rect = self.image.get_rect()
        # position of rect
        self.pos = position
        # actual status of the brick
        self.status = brick_type
        # assign id
        self.id = brick_id
        # add ball sprite to sprite group
        # all_sprites.add(self)
        brick_sprites.add(self)

    def update(self):
        # self.rect = self.pos
        self.rect.x = self.pos[0]
        self.rect.y = self.pos[1]

    def change_status(self, new_status):
        self.status = new_status
        self.image = pygame.image.load(LevelDefines.BRICK_IMAGE[new_status]).convert_alpha()


class Paddle(pygame.sprite.Sprite):
    """
    Class for the paddle object. (Sprite)
    """

    def __init__(self):
        # initialize sprite class object
        pygame.sprite.Sprite.__init__(self)
        # assign image for paddle
        self.image = pygame.image.load("res/img/PaddleBlueMiddle.png").convert_alpha()
        self.rect = self.image.get_rect()
        # initial position
        self.rect.x = WIN_SIZE[0] / 2
        self.rect.y = 4 / 5 * WIN_SIZE[1]
        # initial speed
        self.speed = PADDLE_INITIAL_SPEED
        # add to sprites
        ball_paddle_sprites.add(self)
        # set change
        self.change_x = self.rect.x
        # save previous pos
        self.prev_x = self.rect.x

    def can_move_right(self):
        return self.rect.right < WIN_SIZE[0]

    def can_move_left(self):
        return self.rect.left > 0

    def update(self):
        self.prev_x = self.rect.x
        self.rect.x = self.change_x


# GLOBAL GAME METHODS
def init_bricks(level_num):
    """
    Initializes the bricks of the actual level.

    :param level_num: the actual level.
    """
    # check if level number is valid
    if not 0 <= level_num <= LevelDefines.LEVEL_NUM:
        return

    global bricks_count
    global bricks_gone
    global brick_id_matrix
    global brick_position_matrix_x
    global brick_position_matrix_y

    brick_count_row = 0
    row_count = 0
    bricks_count = 0
    bricks_gone = 0

    # initial height and width of bricks
    brick_height = pygame.image.load(LevelDefines.BRICK_IMAGE[0]).get_rect().height
    brick_width = pygame.image.load(LevelDefines.BRICK_IMAGE[0]).get_rect().width

    # brick offset according to window size and brick defines
    brick_offset_x = ((WIN_SIZE[0] -
                       ((brick_width * LevelDefines.BRICKS_PER_ROW)
                        + (LevelDefines.BRICK_SPACE[0] * LevelDefines.BRICKS_PER_ROW))) / 2) + 3

    id_count = 0

    for brick_type in LevelDefines.BRICK_LAYOUTS[level_num]:
        # calculate position of entire brick
        pos_x = brick_offset_x + (brick_width * brick_count_row) + (LevelDefines.BRICK_SPACE[0] * brick_count_row)
        pos_y = (brick_height * 4) + (LevelDefines.BRICK_SPACE[1] * row_count) + (brick_height * row_count)

        # assign positions to position matrices
        brick_position_matrix_x[row_count][brick_count_row] = pos_x
        brick_position_matrix_y[row_count][brick_count_row] = pos_y

        # assign id to brick id matrix
        brick_id_matrix[row_count][brick_count_row] = id_count
        # create instance of Brick
        brick = Brick(brick_type, [pos_x, pos_y], id_count)
        # update stats
        if brick_type > 0:
            bricks_count += 1
        if brick.status >= LevelDefines.BRICK_UNDESTR:
            bricks_gone += 1
        # next row
        if brick_count_row == LevelDefines.BRICKS_PER_ROW - 1:
            brick_count_row = 0
            row_count += 1
        else:
            brick_count_row += 1
        id_count += 1
        brick_sprites.add(brick)

    print(
        "Bricks added. Level Num: " + str(level_num) + ", count: " + str(bricks_count) + ", gone: " + str(bricks_gone))


def rotate_bricks(level_num):
    """
    Constantly rotates the bricks by a given
    rotate scheme according to the
    actual level number.

    :param level_num: the actual level.
    """
    global last_time_rotate
    global brick_id_matrix
    rotate_scheme = LevelDefines.ROTATE_SCHEME[level_num]
    time = clock.get_time()
    rotate_time_diff = time - last_time_rotate
    last_time_rotate -= time

    try:

        if rotate_time_diff > MAX_FRAME_RATE * 10:
            last_time_rotate = time

            # rotate brick_id_matrix
            for i in range(0, len(rotate_scheme), 4):
                brick_id_matrix = rotate_matrix_by_col_and_row(brick_id_matrix, rotate_scheme[i], rotate_scheme[i + 1],
                                                               rotate_scheme[i + 2], rotate_scheme[i + 3])
            # update bricks pos
            for brick in brick_sprites:
                brick_indices = where(brick_id_matrix == brick.id)
                index_x = int(brick_indices[0])
                index_y = int(brick_indices[1])
                brick.pos = [brick_position_matrix_x[index_x][index_y], brick_position_matrix_y[index_x][index_y]]

    except IndexError:
        print("Couldn't rotate brick id matrix due to index out of bounds!")
        return
    except Exception as rotate_e:
        print("Unknown Error occured! Error: " + str(rotate_e))
        return


def rotate_matrix_by_col_and_row(id_matrix, start_row, start_col, end_row, end_col):
    """
    Rotates one or more certain parts of a 2D matrix
    clockwise.

    The part(s) is (are) given by the start_ to end_row
    and start_ to end_col, basically representing the
    left upper corner and the right bottom corner
    of the rectangle you want to rotate.

    Example:

        Consider the following square made of bricks
        of type 1 and 2:

        [1, 2,
         2, 1]

        To rotate it clockwise the params would be:
        start_row: 0, start_col 1
        end_row: 1, end_col: 2

    :param id_matrix: the 2D array to rotate elements in.
    :param start_row: the starting row.
    :param start_col: the starting column.
    :param end_row: the end row.
    :param end_col: the end column
    :return: the rotated 2D matrix (array).
    """

    # TODO: Maybe add param for number of cycles (rotations)

    tmp_start_row = id_matrix[start_row][end_col]
    tmp_end_row = id_matrix[end_row][start_col]

    # iterate through first and last row
    i = end_col

    while i > start_col:
        # swap elements from right to left
        id_matrix[start_row][i] = id_matrix[start_row][i - 1]
        # swap elements from left to right
        id_matrix[end_row][start_col + end_col - i] = id_matrix[end_row][start_col + end_col - i + 1]
        i -= 1

    # same with columns left and right
    i = end_row

    while i > start_row:
        id_matrix[i][end_col] = id_matrix[i - 1][end_col]
        id_matrix[start_row + end_row - i][start_col] = id_matrix[start_row + end_row - i + 1][start_col]
        i -= 1

    # finally assign saved values
    id_matrix[start_row + 1][end_col] = tmp_start_row
    id_matrix[end_row - 1][start_col] = tmp_end_row
    return id_matrix


def cycle_title_color():
    """
    Fades between different RGB values for certain game
    screen titles.
    """
    global col_cycle_step
    global next_color
    global active_color
    global current_color

    col_cycle_step += 1
    if col_cycle_step < MAX_FRAME_RATE:
        # (y-x)/MAX_FRAME_RATE calculates the amount of change per step required to
        # fade one channel of the old color to the new color
        # source: https://stackoverflow.com/questions/51973441/how-to-fade-from-one-colour-to-another-in-pygame
        current_color = [x + (((y - x) / MAX_FRAME_RATE) * col_cycle_step) for x, y in
                         zip(active_color, next_color)]

    else:
        col_cycle_step = 1
        active_color = next_color
        next_color = next(title_colors)


# DATABASE METHOD(S)
def db_connect():
    """
    Establishs a connection to SQLite DB.
    :return: connection object if success, negative value if not.
    """
    con = -1
    try:
        # create connection to database or create db if nonexistent
        con = sqlite3.connect("highscore.db")
        return con
    except ConnectionError:
        print("It wasn't possible to establish a connection to the database.")
    except Exception as db_e:
        print("Unkown error occured while trying to establish database connection. " + str(db_e))
    return con


def db_create_table(cursor):
    """
    Creates the highscores DB table
    """
    cmd = """
          CREATE TABLE highscores (
          id INTEGER PRIMARY KEY,
          name VARCHAR(3),
          score INTEGER);"""
    cursor.execute(cmd)


def db_enter_highscores(hs_list, cursor):
    """
    Inserts data into highscores table.
    :param hs_list: the data to be inserted.
    """
    for hs, entry in enumerate(hs_list):
        format_str = ''' INSERT INTO highscores (id, name, score)
         VALUES ({id}, '{name}', {score});'''
        cmd = format_str.format(id=hs + 1, name=entry[0], score=entry[1])
        cursor.execute(cmd)


# GAME SCREENS
def game_intro():
    """
    Game Intro screen.
    """
    global is_game_intro
    global is_game
    global is_game_over
    global current_color
    global highscore
    global track_list

    # reset bools
    is_game_over = False
    # reset highscore
    highscore = 0

    # load music
    track_list = listdir(MUSIC_PATH)

    try:

        # load font and render text
        title_font = pygame.font.Font("res/font/Game_Played.otf", 20)
        title_on = title_font.render("PRESS ENTER", True, (244, 131, 66), COL_BACKGROUND).convert_alpha()
        # create surfaces for title text on/off for blinking animation
        blink_rect = title_on.get_rect()
        title_off = pygame.Surface(blink_rect.size)
        # cycle through both surfaces
        blink_surfaces = cycle([title_on, title_off])
        blink_surface = next(blink_surfaces)
        pygame.time.set_timer(TITLE_BLINK_EVENT, MAX_FRAME_RATE * 10)

        # get title text height and width
        text_width = title_on.get_width()
        text_height = title_on.get_height()

        # load title image
        title_img = pygame.image.load("res/img/Coffee.png")
        title_img_rect = title_img.get_rect()
        # initial position
        title_img_rect.x = WIN_SIZE[0] / 2 - title_img_rect.width / 2
        title_img_rect.y = WIN_SIZE[1] / 3 - title_img_rect.height

        # help text and images
        arr_left_img = pygame.image.load("res/img/arrLeft.png")
        arr_right_img = pygame.image.load("res/img/arrRight.png")
        arr_left_img_rect = arr_left_img.get_rect()
        arr_right_img_rect = arr_right_img.get_rect()

        help_font = pygame.font.Font("res/font/Game_Played.otf", 14)

        help_caption_move = help_font.render("MOVE: ", True, (232, 168, 32), COL_BACKGROUND).convert_alpha()
        help_caption_music = help_font.render("MUSIC: ", True, (232, 168, 32), COL_BACKGROUND).convert_alpha()
        help_caption_exit = help_font.render("QUIT: ", True, (232, 168, 32), COL_BACKGROUND).convert_alpha()
        help_caption_start = help_font.render("START: ", True, (232, 168, 32), COL_BACKGROUND).convert_alpha()
        help_descr_music = help_font.render("+/-", True, (226, 216, 195), COL_BACKGROUND).convert_alpha()
        help_descr_exit = help_font.render("ESC", True, (226, 216, 195), COL_BACKGROUND).convert_alpha()
        help_descr_start = help_font.render("SPACE", True, (226, 216, 195), COL_BACKGROUND).convert_alpha()

        arr_left_img_rect.x = WIN_SIZE[0] / 2
        arr_left_img_rect.y = WIN_SIZE[1] - 100

        arr_right_img_rect.x = arr_left_img_rect.x + arr_left_img_rect.width
        arr_right_img_rect.y = arr_left_img_rect.y

        while is_game_intro:
            # event handling
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == TITLE_BLINK_EVENT:
                    blink_surface = next(blink_surfaces)
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        quit()

            # key events
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_RETURN]:
                is_game_intro = False
                is_game = True

            window.fill(COL_BACKGROUND)

            # display blinking title text
            window.blit(blink_surface, (WIN_SIZE[0] / 2 - text_width / 2, WIN_SIZE[1] / 2 - text_height / 2))

            # display title image
            pygame.draw.rect(window, COL_BACKGROUND, title_img_rect)
            window.blit(title_img, title_img_rect)

            # draw help
            text_pos_x = arr_left_img_rect.x - help_caption_move.get_width() - 10
            window.blit(help_caption_move, (text_pos_x, arr_left_img_rect.y))
            pygame.draw.rect(window, COL_BACKGROUND, arr_left_img_rect)
            window.blit(arr_left_img, arr_left_img_rect)
            pygame.draw.rect(window, COL_BACKGROUND, arr_right_img_rect)
            window.blit(arr_right_img, arr_right_img_rect)
            window.blit(help_caption_music, (text_pos_x, arr_left_img_rect.y + help_caption_move.get_height() + 5))
            window.blit(help_descr_music,
                        (arr_right_img_rect.x - 10, arr_left_img_rect.y + help_caption_move.get_height() + 5))
            window.blit(help_caption_start, (text_pos_x, arr_left_img_rect.y + help_caption_move.get_height() * 2 + 10))
            window.blit(help_descr_start,
                        (arr_right_img_rect.x - 10, arr_left_img_rect.y + help_caption_move.get_height() * 2 + 10))
            window.blit(help_caption_exit, (text_pos_x, arr_left_img_rect.y + help_caption_move.get_height() * 3 + 15))
            window.blit(help_descr_exit,
                        (arr_right_img_rect.x - 10, arr_left_img_rect.y + help_caption_move.get_height() * 3 + 15))

            # update screen
            pygame.display.update()
            clock.tick(MAX_FRAME_RATE)

    except FileNotFoundError:
        print("Couldn't load font files.")
        return
    except Exception as gi_e:
        print("Unknown Error occured! Error: " + str(gi_e))
        return


def game_loop():
    """
    Game Loop.
    """
    global actual_level_num
    global is_game_over
    global is_game
    global highscore
    global music_volume

    # set run bool
    running = True

    # set key events on repeat
    pygame.key.set_repeat()

    # init class instances
    paddle = Paddle()
    ball_pos = [paddle.rect.x + paddle.rect.width / 2, paddle.rect.y - 10]
    ball = Ball(ball_pos)
    player = Player()

    # collision counter
    collision_tick = 0
    collision = False

    # setup bricks
    init_bricks(actual_level_num)

    # stop music
    pygame.mixer.music.stop()

    # select new track
    rnd_track = randint(0, len(track_list) - 1)
    pygame.mixer.music.load(MUSIC_PATH + track_list[rnd_track])

    # start music
    pygame.mixer.music.play(0)

    # music volume image
    music_vol_img = pygame.image.load("res/img/sound_on.png")
    music_vol_img_rect = music_vol_img.get_rect()
    music_vol_img_rect.x = WIN_SIZE[0] - music_vol_img_rect.width
    music_vol_img_rect.y = 10

    # main loop
    while running:
        # check if level up
        if bricks_gone >= bricks_count:
            ball.moving = False
            actual_level_num += 1
            brick_sprites.empty()
            if actual_level_num >= LevelDefines.LEVEL_NUM:
                is_game_over = True
                is_game = False

                break;
            else:
                init_bricks(actual_level_num)

        # check if ball out
        if ball.check_ball_out(paddle):
            ball.moving = False
            ball.speed[0] = SPEED_RATE
            ball.speed[1] = -SPEED_RATE
            player.life -= 1
            if player.life < 1:
                is_game_over = True
                is_game = False

        # leave ball at paddle before start
        if not ball.moving:
            ball.rect.x = paddle.rect.x + paddle.rect.width / 2
            ball.rect.y = paddle.rect.y - 10

        # event handling
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            # music playback
            if event.type == SONG_FINISHED:
                rnd_track = randint(0, len(track_list) - 1)
                pygame.mixer.music.load(MUSIC_PATH + track_list[rnd_track])
                pygame.mixer.music.play(0)
            # volume controle
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_KP_PLUS:
                    # raise volume
                    if not music_volume == 1.0:
                        music_volume += 0.1
                    pygame.mixer.music.set_volume(music_volume)
                if event.key == pygame.K_KP_MINUS:
                    # lower volume
                    if not music_volume == 0.0:
                        music_volume -= 0.1
                    pygame.mixer.music.set_volume(music_volume)
                if event.key == pygame.K_ESCAPE:
                    quit()

        # key events
        pressed = pygame.key.get_pressed()
        # move paddle
        if pressed[pygame.K_RIGHT]:
            if paddle.can_move_right():
                paddle.change_x += paddle.speed
        if pressed[pygame.K_LEFT]:
            if paddle.can_move_left():
                paddle.change_x -= paddle.speed
        # start ball
        if pressed[pygame.K_SPACE]:
            if not ball.moving:
                ball.moving = True

        # show score and lives
        font = pygame.font.Font("res/font/Game_Played.otf", 12)
        lives = font.render("LIVES:", True, (216, 210, 195), COL_BACKGROUND).convert_alpha()
        score = font.render("SCORE:", True, (216, 210, 195), COL_BACKGROUND).convert_alpha()
        score_text = font.render(str(player.score), False, (216, 210, 195), COL_BACKGROUND).convert_alpha()
        text_lives_height = lives.get_height()
        text_score_height = score.get_height()

        # rotate bricks according to rotate scheme
        rotate_bricks(actual_level_num)

        # ball movement
        ball.check_bounds()

        ''' Check if ball collided with paddle
            within Framerate. This prevents 
            pygame sprite.collide to fire
            again, causing weird behaviour.'''
        if collision:
            if collision_tick == MAX_FRAME_RATE:
                collision_tick = 0
                collision = False
            else:
                collision_tick += 1

        if not collision:
            # check collision with paddle
            if ball.check_collision_paddle(paddle):
                collision = True
                bounce_effect.play()

            # check collision with bricks
            for brick in brick_sprites:
                if ball.check_collision_brick(brick):
                    collision_brick = True
                    player.score += 1

        # update objects
        ball_paddle_sprites.update()
        brick_sprites.update()

        # deprecated
        # brick_hit_list = pygame.sprite.spritecollide(ball, brick_sprites, False)

        # draw sprites
        window.fill(COL_BACKGROUND)
        brick_sprites.draw(window)
        ball_paddle_sprites.draw(window)
        # draw status text
        window.blit(lives, (10, WIN_SIZE[1] - 20 - text_lives_height / 2))
        window.blit(score, (WIN_SIZE[0] - score.get_width() - 40, WIN_SIZE[1] - 20 - text_score_height / 2))
        window.blit(score_text, (WIN_SIZE[0] - 20, WIN_SIZE[1] - 20 - text_score_height / 2))

        # draw life sprites
        if player.life >= 1:
            life_img = pygame.image.load("res/img/coffee_life.png").convert_alpha()
            life_img = pygame.transform.scale(life_img, (12, 14))
            life_img_rect = life_img.get_rect()

            for i in range(player.life):
                life_img_rect.x = lives.get_width() + 20 + i * 20
                life_img_rect.y = WIN_SIZE[1] - lives.get_height() - 14
                window.blit(life_img, life_img_rect)

        # get volume image
        if music_volume <= 0.0:
            music_vol_img = pygame.image.load("res/img/sound_off.png")
        elif 0.0 < music_volume <= 0.3:
            music_vol_img = pygame.image.load("res/img/sound_on_30.png")
        elif 0.3 < music_volume <= 0.6:
            music_vol_img = pygame.image.load("res/img/sound_on_60.png")
        else:
            music_vol_img = pygame.image.load("res/img/sound_on.png")

        # display volume image
        pygame.draw.rect(window, COL_BACKGROUND, music_vol_img_rect)
        window.blit(music_vol_img, music_vol_img_rect)

        # cap framerate
        clock.tick(MAX_FRAME_RATE)

        # finally update display
        pygame.display.update()

        # end loop if game over
        if is_game_over:
            highscore = player.score
            print("Game Over...Highscore: " + str(highscore))
            del player
            del ball
            del paddle
            running = False


def game_over():
    """
    Game Over screen.
    """
    global is_game_intro
    global is_game_over
    global is_highscore

    # stop music
    pygame.mixer.music.stop()

    # empty sprites
    ball_paddle_sprites.empty()
    brick_sprites.empty()

    try:

        while is_game_over:
            # load font and render text
            font = pygame.font.Font("res/font/Game_Played.otf", 20)
            go_title = font.render("GAME OVER", True, current_color, COL_BACKGROUND).convert_alpha()
            go_descr = font.render("PRESS SPACE", True, (244, 131, 66), COL_BACKGROUND).convert_alpha()

            # event handling
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        quit()

            # key events
            pressed = pygame.key.get_pressed()
            if pressed[pygame.K_SPACE]:
                # is_game_intro = True
                is_highscore = True
                is_game_over = False

            cycle_title_color()

            window.fill(COL_BACKGROUND)

            # display blinking title text
            window.blit(go_title,
                        (WIN_SIZE[0] / 2 - go_title.get_width() / 2, WIN_SIZE[1] / 3 - go_title.get_height() / 2))

            window.blit(go_descr,
                        (WIN_SIZE[0] / 2 - go_descr.get_width() / 2, WIN_SIZE[1] / 2 + go_title.get_height() * 2))

            # update screen
            pygame.display.update()
            clock.tick(MAX_FRAME_RATE)

    except Exception as go_e:
        print("Unknown Error occured! Error: " + str(go_e))
        return


def high_score():
    """
    Highscore screen.
    """
    global is_game_intro
    global is_game_over
    global is_highscore
    global highscore
    global actual_level_num

    # reset level
    actual_level_num = 1
    # highscore name
    hs_name = ''
    # result from highscore DB table as list
    hs_list = []
    # entry possible?
    hs_entered = False
    # establish connection to DB
    con = db_connect()
    # create cursor to work on db
    cursor = con.cursor()

    # check if table already exists
    cursor.execute('''SELECT count(name) FROM sqlite_master WHERE type='table' AND name='highscores' ''')

    # create table highscores if not existing
    if cursor.fetchone()[0] != 1:
        print("Creating database table for highscore list...")
        db_create_table(cursor)

    # fetch data from DB
    cursor.execute(''' SELECT * FROM highscores ''')
    print("Fetching data: ")
    res = cursor.fetchall()
    # insert test entries if there's no data yet
    if len(res) < 1:
        cmd = """INSERT INTO highscores (id, name, score)
                            VALUES (NULL, "M_B", 100);"""
        cursor.execute(cmd)
        cmd = """INSERT INTO highscores (id, name, score)
                            VALUES (NULL, "DEX", 80);"""
        cursor.execute(cmd)
        cmd = """INSERT INTO highscores (id, name, score)
                                    VALUES (NULL, "ROY", 60);"""
        cursor.execute(cmd)
        cmd = """INSERT INTO highscores (id, name, score)
                                    VALUES (NULL, "JAX", 40);"""
        cursor.execute(cmd)
        cmd = """INSERT INTO highscores (id, name, score)
                                    VALUES (NULL, "DEX", 10);"""
        cursor.execute(cmd)
        # commit changes
        con.commit()
        # fetch again
        cursor.execute(''' SELECT * FROM highscores ''')
        print("Fetching data: ")
        res = cursor.fetchall()

    # commit changes
    con.commit()

    # add entries to hs list
    for r in res:
        hs_list.append((r[1], r[2]))

    # lowest highscore
    min_hs = res[4][2]

    # load font and render text
    font_title = pygame.font.Font("res/font/Game_Played.otf", 20)
    font_name = pygame.font.Font("res/font/Game_Played.otf", 14)
    font_entry = pygame.font.Font("res/font/Game_Played.otf", 18)

    hs_descr = font_name.render("Enter name:", True, (216, 210, 195),
                                COL_BACKGROUND).convert_alpha()

    hs_to_intro_descr = font_title.render("PRESS SPACE", True, (244, 131, 66), COL_BACKGROUND).convert_alpha()

    try:
        while is_highscore:
            # render title here to get color cycle
            hs_title = font_title.render("HIGHSCORE:", True, current_color, COL_BACKGROUND).convert_alpha()

            # event handling
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
                if event.type == pygame.KEYDOWN and highscore > min_hs and not hs_entered:
                    if event.key == pygame.K_RETURN:
                        # drop table
                        cursor.execute(''' DROP TABLE highscores ''')
                        # create new table
                        db_create_table(cursor)
                        # insert new score
                        hs_list.append((hs_name, highscore))
                        # sort
                        hs_list = sorted(hs_list, key=lambda res: res[1], reverse=True)
                        # cut last entry
                        hs_list = hs_list[:5]
                        # enter new values in table
                        db_enter_highscores(hs_list, cursor)
                        con.commit()
                        # set entered
                        hs_entered = True
                    elif event.key == pygame.K_BACKSPACE:
                        hs_name = hs_name[:-1]
                    else:
                        if not len(hs_name) > 2:
                            hs_name += event.unicode
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_SPACE:
                        is_game_intro = True
                        is_highscore = False
                        is_game_over = False
                    if event.key == pygame.K_ESCAPE:
                        quit()

            if hs_entered:
                # fetch new data
                cursor.execute(''' SELECT * FROM highscores ''')
                res = cursor.fetchall()

            # color cycle
            cycle_title_color()

            # create surface for entered text
            hs_name_surface = font_name.render(hs_name, True, (216, 210, 195), COL_BACKGROUND).convert_alpha()

            window.fill(COL_BACKGROUND)

            # display highscore entries
            counter = 0
            for r in res:
                # create surface for highscore entry
                hs_entry_surface = font_entry.render(str(r[0]) + ":  " + str(r[1]) + "....." + str(r[2]), True,
                                                     (83, 87, 214), COL_BACKGROUND).convert_alpha()
                window.blit(hs_entry_surface,
                            (WIN_SIZE[0] / 2 - hs_entry_surface.get_width() / 2,
                             WIN_SIZE[1] / 5 + hs_title.get_height() + counter * hs_entry_surface.get_height()))
                counter += 1

            # display text
            window.blit(hs_title,
                        (WIN_SIZE[0] / 2 - hs_title.get_width() / 2, WIN_SIZE[1] / 5 - hs_title.get_height() / 2))

            if highscore > min_hs and not hs_entered:
                window.blit(hs_descr, (20, WIN_SIZE[1] - 20 - hs_descr.get_height()))
                window.blit(hs_name_surface,
                            (WIN_SIZE[0] - 30 - hs_descr.get_width(), WIN_SIZE[1] - 20 - hs_descr.get_height()))
            else:
                window.blit(hs_to_intro_descr,
                            (WIN_SIZE[0] / 2 - hs_to_intro_descr.get_width() / 2, WIN_SIZE[1] - 100))

            # update screen
            pygame.display.update()
            clock.tick(MAX_FRAME_RATE)

    except FileNotFoundError:
        print("Font for text not found!")
        return

    except Exception as hs_e:
        print("Unknown Error occured! Error: " + str(hs_e))
        return

    con.close()


# main logic
def main():
    while is_running:
        if is_game_intro:
            # start screen
            game_intro()

        if is_game:
            # main game loop
            game_loop()

        if is_game_over:
            game_over()

        if is_highscore:
            high_score()

    pygame.quit()


if __name__ == "__main__":
    main()
