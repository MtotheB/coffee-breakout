# Coffee-Break-Out 

A breakout clone based on pygame.

## Installation 

*  Make sure you're using Python 3.x
*  Import/Install the required dependencies 
   - either by using **pip3 install -r requirements.txt** or
   - by importing them manually into your project
     (see the import section in **CoffeeBreakOut.py** for details)
*  run the game 
   - using either an IDE (e.g. PyCharm) or
   - by CLI *python CoffeeBreakOut.py*

## Documentation

There's a project documentation available for download under the following link:

[Coffee-Break-Out Documentation](https://gitlab.com/MtotheB/coffee-breakout/raw/master/html/CoffeeBreakOut.html?inline=false)